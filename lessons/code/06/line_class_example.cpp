// `Line` class example
//
// Using some member functions of the `Line` class: this program displays a
// rectangle using only two lines. These lines are moved and _displayed_ as
// needed.
//
// > **Note:**
// >
// > This program needs the _ccc_graphics_ files provided by the author of our
// > textbook. You also need to perform additional steps in your Visual Studio
// > or Xcode setup in order to compile this example.
//
// UCLA Pic 10A Prof. Salazar


#include "ccc_win.h"    // cwin, Line(...), Line::move(...), Point

int ccc_win_main(){  
    Point top_left( 1, 5 );
    Point bottom_left( 1, 3 );
    Point bottom_right( 2, 3 );

    Line horizontal( bottom_left, bottom_right );
    Line vertical( bottom_left, top_left );

    cwin << horizontal << vertical;

    horizontal.move( 0, 2 );
    vertical.move( 1, 0 );

    cwin << horizontal << vertical;

    return 0;
}

/**
   Sample output: N/A
**/
