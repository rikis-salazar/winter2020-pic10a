#include<iostream>
using namespace std;

int main() {
   int a = 2;
   cout << "a = " << a << endl;
   /* if ( a == 3 ) <<-- This should be the correct statement */
   if ( a = 3 ) // Logic error. Very common!.
      cout << "The value is 3." << endl;
   else
      cout << "The value is not 3." << endl;

   return 0;
}
