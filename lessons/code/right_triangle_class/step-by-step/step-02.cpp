#include "ccc_win.h"

using namespace std;


// This is the interface of our class...  public information "out in the open".
class RightTriangle{
  public:
    RightTriangle();
    void draw() const;

  private:
    Point main_corner;
    double width;
    double height;

  public:
    double area;


  public:
    // ***** OTHER CONSTRUCTORS *****
    
    // If only a point is specified [by the programmer] we set the width to 2,
    // and the height to 1 (just like in the default constructor).
    RightTriangle( Point c );

    // If only the dimensions are specified, we set the main corner at the
    // origin (just like in the default constructor).
    RightTriangle( double w, double h );

    // Alternatively, the programmer could specify both the dimensions, as well
    // as the main corner.
    RightTriangle( Point c, double w, double h );

    // Even the area could be [incorrectly?!] specified.
    RightTriangle( Point c, double w, double h, double a );
};




// Member function: Default constructor
// Usage:
//     RightTriangle r;
// ...
RightTriangle::RightTriangle(){
    main_corner = Point(0,0);
    width = 2.0;
    height = 1.0;
    area = ( width * height ) / 2;
}

// Member function: `draw()`
// Usage:
//     some_triangle.draw();
// ...
void RightTriangle::draw() const {
    Point aux1 = main_corner;
    Point aux2 = main_corner;

    aux1.move(width,0);
    aux2.move(0,height);

    cwin << Line( main_corner, aux1 );
    cwin << Line( aux1, aux2 );
    cwin << Line( aux2, main_corner );
}




// Member function: One-parameter constructor (constructor overload)
// Usage:
//     RightTriangle r( Point(1, 2.3) );
//
//  Note that the main corner is being provided as a local point named `c`. We
//  simply pass this along to the main corner; then populate the rest of the
//  fields with default values.
RightTriangle::RightTriangle( Point c ){
    // Data provided in the function call
    main_corner = c;
    
    // Default (non-provided) values.
    width = 2.0;
    height = 1.0;
    area = ( width * height ) / 2;
}

// Member function: Two-parameter constructor (constructor overload)
// Usage:
//     RightTriangle r( 5.43, 2.1 );
//
//  Note that the width and the height are being provided as a local variables
//  named `w`, and `h`, respectively. We simply pass this along to the
//  corresponding class fields; then populate the rest of the fields with
//  default values.
RightTriangle::RightTriangle( double w, double h ){
    // Data provided in the function call
    width = w;
    height = h;
    
    // Default (non-provided) values.
    main_corner = Point(0,0);
    area = ( width * height ) / 2;
}

// Member function: Three-parameter constructor (constructor overload)
// Usage:
//     RightTriangle r( Point(-1,-3), 3.14, 1 );
//
// Blah, blah, blah...
RightTriangle::RightTriangle( Point c, double w, double h ){
    // Data provided in the function call
    main_corner = c;
    width = w;
    height = h;
    
    // Default (non-provided) values.
    area = ( width * height ) / 2;
}

// Member function: Four-parameter constructor (constructor overload)
// Usage:
//     RightTriangle r( Point(-1,-3), 3.14, 1 , 0 );  // Area = 0 ???!!!
//
// Blah, blah, blah...
RightTriangle::RightTriangle( Point c, double w, double h, double a ){
    // Data provided in the function call
    main_corner = c;
    width = w;
    height = h;
    area = a;
}




// At this point, the interface...
int ccc_win_main() {
    // Create and draw a default triangle.
    RightTriangle triangle1;
    triangle1.draw();

    // Create and draw a triangle at the point center2.
    Point center2(-5,5);
    RightTriangle triangle2( center2 );
    triangle2.draw();

    // Create and draw a triangle with dimension width3, and height3.
    double width3 = 3.14;
    double height3 = 4.13;
    RightTriangle triangle3( width3, height3 );
    triangle3.draw();

    // Create and draw a triangle at (-5,-5) with dimensions 8.42, -2.48.
    RightTriangle triangle4( Point(-5,-5), 8.42, -2.48 );
    triangle4.draw();

    // Create and draw a triangle at (5,-5) with dimensions -8.42, -2.48 and
    // area determined by its dimensions.
    RightTriangle triangle5( Point(5,-5), -8.42, -2.48, -8.42 * -2.48 );
    triangle5.draw();

    // Create and draw a triangle at (5,5) with dimensions -2, 1 and arbitrary
    // area specified by the programmer.
    center2.move(10,0);
    RightTriangle triangle6( center2, -2, 1, -2019 );
    triangle6.draw();

    return 0;
}




/**
    // INCORRECT IMPLEMENTATION OF CLASS CONSTRUCTORS

    RightTriangle::RightTriangle(){
        Point main_corner = Point(0,0);
        double width = 2.0;
        double height = 1.0;
        double area = ( width * height ) / 2;
    }
    RightTriangle::RightTriangle( Point c ){
        Point main_corner = c;
        double width = 2.0;
        double height = 1.0;
        double area = ( width * height ) / 2;
    }
    RightTriangle::RightTriangle( double w, double h ){
        Point main_corner = Point(0,0);
        double width = w;
        double height = h;
        double area = ( width * height ) / 2;
    }
    RightTriangle::RightTriangle( Point c, double w, double h ){
        Point main_corner = c;
        double width = w;
        double height = h;
        double area = ( width * height ) / 2;
    }
    RightTriangle::RightTriangle( Point c, double w, double h, area a ){
        Point main_corner = c;
        double width = w;
        double height = h;
        double area = a;
    }

**/
