#include "ccc_win.h"

using namespace std;


// This is the interface of our class...  public information "out in the open".
class RightTriangle{
  public:
    RightTriangle();
    void draw() const;

  private:
    Point main_corner;
    double width;
    double height;

  public:
    double area;
};



// Member function: Default constructor
//
// This function is used when we write a statement of the form
//     RightTriangle r;
//
//  Note that we are not providing information about what data we want "in the
//  triangle". This is why the function is called the default constructor: if no
//  info is provided, then we will default to a set of options that we specify
//  right within this function.
//
//  In this case, we set `main_corner` to be the origin; `width` is set to 2,
//  and `height` is set to 1. These values are completely arbitrary; I chose
//  them because ~~I am the instructor~~ the area of the triangle turns out to
//  be one.
RightTriangle::RightTriangle(){
    // Note that all of the member variables (main_corner, width, height, and
    // area) are already declared in the class interface (lines 13, 14, 15, and
    // 18). This means that **we do not need to redeclare them here**. If we do
    // so, we will have two different sets of variables with different scopes.
    // Moreover, the redeclared variables will _shadow_ the class fields which
    // are the ones we want to have their values set.
    main_corner = Point(0,0);
    width = 2.0;
    height = 1.0;
    area = ( width * height ) / 2;
    // > Scroll all the way down to the bottom to compare this correct
    // > definition of the constructor with one where local variables shadow
    // > the class fields.
}




// Member function: `draw()`
//
// This function is used when we write a statement of the form
//     some_triangle.draw();
//
// Recall that functions that focus on a specific task provide greater
// flexibility when programming. In this case, we will draw a triangle by
// drawing lines between tree points: the `main_corner`, and a couple of
// auxiliary points that will initially be set at the same location as the main
// corner, and will later be positioned at their right locations.
void RightTriangle::draw() const {
    // Auxiliary points initially set "on top" of the main corner.
    Point aux1 = main_corner;
    Point aux2 = main_corner;

    // The auxiliary points are moved to the correct locations...
    aux1.move(width,0);
    aux2.move(0,height);

    // ... and finally the triangle is drawn.
    cwin << Line( main_corner, aux1 );
    cwin << Line( aux1, aux2 );
    cwin << Line( aux2, main_corner );
}




// At this point, the interface...
int ccc_win_main() {
    // We NO LONGER draw a circle... instead we test our functions.
    // cwin << Circle(Point(0,0), 5);

    RightTriangle triangle1;   // <-- Calls the default constructor.
    triangle1.draw();          // <-- No need to explain... it is "readable".

    return 0;
}



/**
    // INCORRECT IMPLEMENTATION OF THE DEFAULT CONSTRUCTOR

    RightTriangle::RightTriangle(){
        Point main_corner = Point(0,0);
        double width = 2.0;
        double height = 1.0;
        double area = ( width * height ) / 2;
    }

    // In line 101, a local point called `main_corner` is "created" at the
    // origin. This local point "steals" the data that is meant to be passed
    // onto the class field `main_corner` (declared in line 13).
    //
    // > NOTE: In this specific scenario the class field is also "created" at
    // > the origin. However, this is because the default constructor for the
    // > Points class, sets the coordinates at (0,0).
    //
    // In lines 102 -- 104, local variables `width`, `height`, and `area` are
    // "created", they then "steal" the values we intended to pass onto the
    // class fields.
    //
    // > NOTE: In this specific scenario the class fields are left with nothing
    // > but **garbage values**. This is because primitive types (_e.g.,_ int,
    // > double, bool, etc) are not initialized to default values.
**/
