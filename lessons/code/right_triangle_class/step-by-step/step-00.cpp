#include "ccc_win.h"

using namespace std;

// This is the interface of our class. We agreed that a point (main_corner), as
// well as two quantities (width and height), completely determine a Right
// Triangle.
//
// The private section of our class contains the fields described above, namely:
//     Point main_corner;
//     double width;
//     double height;
//
// The public section contains for now just a small wish list: we want to be
// able to "construct" a triangle (but do not necessarily "draw it during
// construction"); and we want to be able to "draw" it.
//
// We also add yet another public section containing one field:
//     double area;
//
// This is done to illustrate the dangers of leaving public information "out in
// the open".

class RightTriangle{
  public:
    RightTriangle();
    void draw() const;

  private:
    Point main_corner;
    double width;
    double height;

  public:
    double area;
};



// At this point, the interface is syntactically correct, but the program is
// non-functional. This is because the member function definitions, that is the
// code for the constructor as well as the `draw()` function, are missing.
int ccc_win_main() {
    // We draw a circle, just to test our graphics setup.
    cwin << Circle(Point(0,0), 5);

    return 0;
}
