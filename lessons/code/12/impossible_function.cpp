/* *********************************************
   Ricardo Salazar
   February 9, 2015

   Example:
   Example of function declarations.

   Keywords: 
   functions, declarations, recursion.

   #ThisMakesNoSense #EasterEgg #Prison1 #Prison2
   #MonteCarlo #OneMoreHashTag #CrypticMessages
   ********************************************* */

#include <iostream>
using namespace std;


// Function declarations. Whitout them it is impossible to successfully compile
// the program. Why???
void function1(int );
void function2();


/* *********************************************
   function1  
           A simple function that displays a message or calls a second function
           depending on what values is being passed onto the function.

   Parameters:
      n    a integer n. The value n = 0 terminates the recursion process.

   Returns: No return.
   ********************************************* */
void function1( int n ){
    if ( n == 0 )
        function2();
    else
        cout << "Bye\n";

    return;
}


/* *********************************************
   function2  
        A simple function that calls the function that called it, initiating a
        recursion process. 

        The recursion terminates because the stopping condition is passed onto
        the function being called.

   Parameters:
        No parameters;

   Returns:
        No return.
   ********************************************* */
void function2(){
    function1( 0 );
    return;
}


/* *********************************************
   main  
   ********************************************* */
int main (){
    function1( 1 );
    return 0;
}
