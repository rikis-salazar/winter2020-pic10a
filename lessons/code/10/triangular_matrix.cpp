/* *****************************************************
   Ricardo Salazar
   February 2, 2015

   Example:
   Displaying a triangular matrix.

   Keywords: 
   Nested loops. 
   ***************************************************** */
#include <iostream>
using namespace std;

int main(){

   for(int row=1 ; row<=5 ; row++){
      for (int col=1 ; col <=5 ; col++){
         if ( col > row )
            cout << 0 << "\t";
         else 
            cout << row - col + 1 << "\t";
      }
      cout << endl;
   }

   return 0;
}

