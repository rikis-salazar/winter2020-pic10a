/* *****************************************************
   Ricardo Salazar
   January 30, 2014

   Example:
   Input validation.

   Keywords: 
   cin.fail, loops.
   ***************************************************** */
#include<iostream>
using namespace std;

int main(){
   int a;
   cout << "Please type an integer: ";
   if ( cin >> a ) {
      cout << "Thanks!\n";
      return 0;
   }
   else{
      cout << "Menso! (You dummy!)\n";
      return 1;
   }
}
