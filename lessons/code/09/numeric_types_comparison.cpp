/* ************************************************
   Ricardo Salazar
   January 27, 2015

   Example: 
   Comparing numeric types (int and double)

   Keys: 
   inexact binary repesentation, 
   inequalities vs equalities as stopping conditions.
   ************************************************ */
#include <iostream>
using namespace std;

int main() {
   // Upexpected results with either of the 
   // following two lines.
   //int r = static_cast<int>( 4.35 * 100 );   
   double r =  4.35 * 100 ;

   if ( 435 - r  == 0 )
      cout << "You should be here..."; 
   else
      cout << "... but you are here instead!" << endl; 

   return 0;
}
