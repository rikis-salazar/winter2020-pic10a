/* ***************************************
   Ricardo Salazar  
   January 29, 2015
   
   Example: 
   The factorial of a positive number.

   Keywords:
   while loop, accumulators.
   *************************************** */
#include <iostream>
using namespace std;

int main(){
   int n;
   int factorial=1;

   cout << "Number? ";
   cin >> n;
   cout << "The factorial of " << n << " is ";

   while ( n >=0 ) {
      factorial *= n;
      n--;
   }
   
   cout << factorial << endl;

   return 0;
}
