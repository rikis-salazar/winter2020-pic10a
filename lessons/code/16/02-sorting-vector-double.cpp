#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    vector<double> v(5);
    v[0]=1.23;
    v[1]=0.21;
    v[2]=34.1;
    v[3]=-10;
    v[4]=17.2;

    sort( v.begin() , v.end() );  // It needs the < operator
    for ( int i=0 ; i < v.size() ; i++ )
        cout << v[i] << " ";

    cout << endl;
    return 0;
}
