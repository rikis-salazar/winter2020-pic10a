/* *********************************************
   Ricardo Salazar
   February 16, 2015

   Example:
      The Product class

   Keywords: 
      Classes, interface, implementation, 
      implicit parameter, explicit parameter,
      certification.
   ********************************************* */

#include <iostream>
#include <string>
using namespace std;

// Product interface
class Product{
  public:
    Product();

    // The constructor below is not used but we implemented it and provided
    // examples of its possible use during lecture.
    Product( string n, double p, int s );

    void read();
    void print() const;
    bool is_better_than( Product b ) const;

  private:
    string name;
    double price;
    int score;
};



// **********************************************
// Function definitions
// **********************************************

// Creates a default Product.
// Note: to update the private fields, the mutator `read()` needs to be called.
Product::Product(){
    name = "No item";
    price = 1.0;
    score = 0;
}


// Creates a product with the information specified in the parameters
Product::Product( string n, double p, int s ){
    name = n;
    price = p;
    score = s;
}


// Mutator: Prompts user for a string (the name), a double (the price) and an
// integer value (the score); and sets the private fields to those provided by
// the user. Note: a dummy variable is used to clear the buffer (cin).
void Product::read(){
    cout << "Please enter the model name: ";
    getline(cin, name);
    cout << "Please enter the price: ";
    cin >> price;
    cout << "Please enter the score: ";
    cin >> score;
    string remainder;
    getline(cin, remainder);
    return;
}


// Accessor: Displays the contentents of the private fields.
void Product::print() const {
    cout << "Name: " << name << endl;
    cout << "Price: " << price << endl;
    cout << "Score: " << score << endl;
    return;
}


// Function that compares to Products based on the ratio score/price.  We avoid
// division by zero by considering three separate cases
//
// Keywords: implicit/explicit parameters
bool Product::is_better_than(Product b) const {
    if ( b.price == 0 )
        return false;
    if ( price == 0 )
        return true;

    return score/price > b.score/b.price;
}


// main routine (a.k.a. the driver)
int main(){
    string response;
    string dummy;

    Product next;
    Product best;

    do {
        next.read();
        if ( next.is_better_than(best) ){
            best = next;
        }
        cout << "More products (y/n)? ";
        cin >> response;
        getline(cin,dummy);

    } while(response == "y");

    cout << "The best product is: ";
    best.print();

    return 0;
}
