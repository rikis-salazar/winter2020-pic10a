// Variables: how not to name them.
// UCLA Pic 10A
// Prof. Salazar


#include <iostream>     // std::cout

using namespace std;

int main() {
    int x1 = 8;
    int zz = 4;
    int _a = 3;

    double cC = x1 * 0.01 + zz * 0.10 + _a * 0.25;
    
    cout << "Total value = " << cC << '\n';

    return 0;
}

/**
   OUTPUT:
  
       Total value = 1.23
**/
