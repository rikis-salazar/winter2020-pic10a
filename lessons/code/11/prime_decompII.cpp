/* *****************************************************
   Ricardo Salazar
   February 2, 2015

   Example:
   The 'complex' problem of prime factorization.
   The repetitions have been eliminated.

   Keywords: 
   functions, return values, predicates. 
   ***************************************************** */
#include <iostream>
using namespace std;


// isPrime  test to determine whether a positive number is prime.
//
// Parameters:
//    n    a positive prime.
//
// Returns: true if n is prime, false otherwise.
bool isPrime( int n ){
    // Integers less than 2 are not primes 
    if ( n <= 1 ) 
        return false;

    // 2 is prime
    if ( n == 2 ) 
        return true;

    // 2 is the only even prime. If n is even bigger than 2, it is not prime.
    if ( n % 2 == 0 ) 
        return false;

   // If we reach this point we have and odd integer bigger or equal to three.
   // Check for divisors starting with 3. It suffices to test numbers smaller
   // than sqrt(n). This is a theorem from math!
    int k = 3;
    while ( k * k <= n ){  
        if ( n % k == 0 ) 
            return false;
        k = k + 2;
    }
   
   // If we reach this point we have a positive odd integer bigger than 3 with
   // no divisors other than 1 and itself. That is, we have a prime number.
   return true;
}


int main(){
    // Test cases: uncomment one line at a time.
    // int N = 360;
    // int N = 10;
    int N = 132;

   // See lesson 11 for an explanation of this block.
    for ( int i = 2 ; i <= N ; i++ ){
        int repetition = 1;
        while( isPrime(i)  &&  N % i == 0 ){
            if ( repetition == 1 )
                cout << i << " ";
            N /= i;
            repetition++;
        }
        if ( isPrime(i) )
            cout << endl;
    }
   
    return 0;
}
