/* *****************************************************************
   Ricardo Salazar 
   February 9, 2015 
   
   Example: 
   Area under y = x^2 using a monte carlo 
   simulation.
   
   Keywords: 
   monte carlo, simulation, random numbers

   See 
   http://en.wikipedia.org/wiki/Monte_Carlo_method#Integration
   for an explanation of the idea behind this
   method.
   ***************************************************************** */

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

const int MAX_RUNS = 1e6;

int main(){
    double x,y;
    srand( static_cast<int>( time(0) ));

    int hit = 0;
    for ( int n = 1 ; n <= MAX_RUNS ; n++ ){
        // produces a random number between 0 and 1.
        x = static_cast<double>(rand())/RAND_MAX;
        y = static_cast<double>(rand())/RAND_MAX;
        if ( y <= x*x )
            hit++;
    }

    cout << "Hits = " << hit 
         << " out of " << MAX_RUNS <<".\n";
    cout << "Area = " << static_cast<double>(hit)/MAX_RUNS << endl;
}
