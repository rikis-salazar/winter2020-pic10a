// The `string` class & the `char` primitive type:
//
// Examples of variables, types, objects and classes.
//
// UCLA Pic 10A
// Prof. Salazar

#include <iostream>
#include <string>       // std::string, std::getline, substr

using namespace std;


int main() {

    // 'char' can store only one character (even a special one)
    char c = 'R';
    char emptySpace = ' ';
    char two = '2';
    char newLine = '\n';
    // char emptyChar = '';  // Error! `char` needs a character

    cout << c << emptySpace << two << newLine ;


    string dummy;            // OK! `string` objects can store zero characters
    string initial = "R";
    string nickname = "Rikis";
    string firstName = "Ricardo";
    string lastName = "Salazar\n";
    string emptyString = ""; // Same contents as dummy (above)


    // Demo of member functions:
    //     - `+` to concatenate strings
    //     - `getline` to "pause" the program (ie., capture the 'enter')
    //     - `substr` to extract a sequence of characters in a string

    cout << initial << emptyString << nickname + "\n" ;
    cout << "(Press enter)";
    getline(cin,dummy);
    cout << "My name is " << firstName + lastName << ". Oops!\n";
    cout << "(Press enter)";
    getline(cin,dummy);

    cout << "My name is " << firstName + emptySpace + lastName 
         << ". Did I do it again?" << endl;
    cout << "(Press enter)";
    getline(cin,dummy);

    cout << "My name is " 
         << firstName + emptySpace + lastName.substr(0,lastName.length() - 1) 
         << ". Better!" << endl;
    cout << "(Press enter)";
    getline(cin,dummy);

    cout << "My initials are: " << initial << emptySpace 
         << lastName.substr(0,1) << newLine;

    return 0;
}

/**
   Sample output:

    R 2
    RRikis
    (Press enter)
    My name is RicardoSalazar
    . Oops!
    (Press enter)
    My name is Ricardo Salazar
    . Did I do it again?
    (Press enter)
    My name is Ricardo Salazar. Better!
    (Press enter)
    My initials are: R S
**/
