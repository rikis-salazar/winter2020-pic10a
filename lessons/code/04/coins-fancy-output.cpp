// Formatted output:
//
// Reads 4 integer values corresponding to pennies (1 cent), nickels (5 cents),
// dimes (10 cents), and quarters (25 cents); then displays the total amount of
// money in dollars.
//
// The output is nicely formatted.
//
// UCLA Pic 10A
// Prof. Salazar

#include <iostream>     // std::fixed, std::left, std::right
#include <iomanip>      // std::setw, std::setprecision,

using namespace std;

int main() {

    // Read input from user
    int pennies, nickels, dimes, quarters;

    cout << "How many pennies do you have? ";
    cin >> pennies;
    cout << "How many nickels do you have? ";
    cin >> nickels;
    cout << "How many dimes do you have? ";
    cin >> dimes;
    cout << "How many quarters do you have? ";
    cin >> quarters;


    // Constants to avoid magic numbers & total amount in dollars
    const double VALUE_PENNY = 0.01;
    const double VALUE_NICKEL = 0.05;
    const double VALUE_DIME = 0.1;
    const double VALUE_QUARTER = 0.25;

    double total = pennies * VALUE_PENNY + nickels * VALUE_NICKEL
                 + dimes * VALUE_DIME + quarters * VALUE_QUARTER;


    // Output setup: width of all columns
    const double WIDTH = 10;

    // Output setup: decimal precision and trailing zeros [if needed]
    cout << fixed << setprecision(2);

    // Output setup: text justification
    //     left:  pushes string to the left
    //     right: pushes string to the right
    //
    // Note: order does matter. First set length of column, then set
    // justification, and finally set the contents. Repeat as needed.
    cout << setw(WIDTH) << left << "Coin name"
         << setw(WIDTH) << right << "Quantity"
         << setw(WIDTH) << "Amount" << "\n";

    cout << setw(WIDTH) << left << "Penny"
         << setw(WIDTH) << right << pennies
         << setw(WIDTH) << pennies * VALUE_PENNY << "\n";

    cout << setw(WIDTH) << left << "Nickel"
         << setw(WIDTH) << right << nickels
         << setw(WIDTH) << nickels * VALUE_NICKEL << "\n";

    cout << setw(WIDTH) << left << "Dime"
         << setw(WIDTH) << right << dimes
         << setw(WIDTH) << dimes * VALUE_DIME << "\n";

    cout << setw(WIDTH) << left << "Quarter"
         << setw(WIDTH) << right << quarters
         << setw(WIDTH) << quarters * VALUE_QUARTER << "\n";

    cout << "The total value of your coins is $"
         << total << " USD.\n";

   return 0;
}

/**
   Sample output:

    How many pennies do you have? 5
    How many nickels do you have? 8
    How many dimes do you have? 2
    How many quarters do you have? 3
    Coin name   Quantity    Amount
    Penny              5      0.05
    Nickel             8      0.40
    Dime               2      0.20
    Quarter            3      0.75
    The total value of your coins is $1.40 USD.
**/
