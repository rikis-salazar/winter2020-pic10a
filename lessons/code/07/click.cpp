// Graphics classes example
//
// Basics of input handling via mouse clicks.
//
// > **Note:**
// >
// > This program needs the _ccc_graphics_ files provided by the author of our
// > textbook. You also need to perform additional steps in your Visual Studio
// > or Xcode setup in order to compile this example.
//
// UCLA Pic 10A Prof. Salazar


#include "ccc_win.h"    // cwin, cwin.get_string(...), cwin.get_mouse(...),
                        // Point(...), Circle(...), Message(...)

int ccc_win_main() {
    // Prompt input [text] from user
    string name = cwin.get_string("Please type your name: ");

    // Simple shape in viewing window (to make things pretty?)
    Circle c( Point(0, 0), 1);
    cwin << c;

    // Prompt input [click] from user
    Point clicked_here = cwin.get_mouse("Please click inside the circle.");

    // Interact with user (output)
    cwin << clicked_here << Message(clicked_here, name + ", you clicked here");

    return 0;
}

/**
   Sample output: N/A
**/
