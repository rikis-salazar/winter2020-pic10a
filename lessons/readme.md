# Course lessons (slides)

> Note:
> 
> I will not be able to lecture during the first week of classes; instead
> professor _Hanqin Cai_ will be teaching my classes. Since he is also teaching
> Pic 10A, it might be easier for him to use his own material (which might only
> be available online for students enrolled in his class). Regardless of his
> choice, I am providing `pdf` files corresponding to the set of slides that I
> have used in the past during the first few sessions of the quarter. Once
> again, do keep in mind that the contents of these lessons might differ from
> what is presented to you during the first week of classes.
>
> > *UPDATE 01/13:*
> >
> > I've been told professor _Hanqin_ did use the set of _slides_ posted here.

The first three sessions (Lessons 1 through 3) were prepared using LibreOffice,
an open source _Office Suite_. The actual presentations are saved in `.odp`
files, and even though they can be converted to other formats (_e.g.,_ Microsoft
Office&reg; Power Point&reg;), sometimes the resulting set of slides differs a
lot from the original set. For this reason, only the `.pdf` versions of these
documents are available for you.

*   [Lesson 1][l1]: _Hello world!_, line by line ([practice code][c01]).
*   [Lesson 2][l2]: Variables, and Input/Output ([practice code][c02]).
*   [Lesson 3][l3]: Arithmetic & casting ([practice code][c03]).

The rest of the lessons were prepared using Google Slides. As we discuss their
contents during lecture, I will provide links to the actual slides. Once a set
of slides has been thoroughly discussed, a `.pdf` file will be made available to
the class.

*   [Lesson 4][l4]: String classes, and the `string` class ([slides][l4],
    [`.pdf` file][l4-pdf], [practice code][c04]).

*   [Lesson 5][l5]: Classes: `std::string`, and the `ccc` classes[^one]
    ([slides][l5], [`.pdf` file][l5-tempo-pdf], [practice code][c05]).

    > _Notes:_
    >
    > -   The `.pdf` file provided here does not contain the slides where the
    >     `ccc` classes are discussed as these classes will not be included in
    >     the midterm nor the final exam. 

**Important:**  We will _skip_ lessons 6 and 7 for now. I do not have a windows
computer that I can use to show you how to set up the _graphics_ libraries.
After I get a loaner from the _bugs office_, we will revisit these topics.

*   [Lesson 8][l8]: Decisions I: The `if` statement ([slides][l8],
    [`pdf` file][l8-pdf], [practice code][c08]).

*   [Lesson 9][l9]: Comparing data types, De Morgan's laws, the `while` loop
    ([slides][l9], [`pdf` file][l9-pdf], [practice code][c09]).

    > _Errata:_
    >
    > Somewhere within the `.pdf` file there is a reference to an example at
    > _CCLE Lesson 9_. All examples are now hosted in the [Practice code (by
    > lesson number)][practice] section of the PIC course website.


*   [Lesson 10][l10]: More loops: `do-while`, `for`. Decisions II: the `switch`
    statement ([slides][l10], [`pdf` file][l10-pdf], [practice code][c10]).


**Important:**  Here are lessons 6 and 7 that we skipped earlier.

*   [Lesson 6][l6]: Graphing I. Introduction to graphing ([slides][l6],
    [`pdf` file][l6-pdf], [practice code][c06]).

    > _Errata:_
    >
    > On page 5 of the `.pdf` file, the third bullet point should read: Do not
    > use `using namespace std`. Why not?
    >
    > -   [Click here][ccc_time] to access _non-graphics `ccc` files_
    >     corresponding to the `Time`, and `Employee` classes.

*   [Lesson 7][l7]: Graphing II. More complex shapes ([slides][l7],
    [`pdf` file][l7-pdf], [practice code][c07]).


And after this brief _graphics detour_, let us go back to _regular_ C++ topics.

*   [Lesson 11][l11]: Functions ([slides][l11], [`pdf` file][l11-pdf], [practice
    code][c11]).

*   [Lesson 12][l12]: Value and reference parameters ([slides][l12], [`pdf`
    file][l12-pdf], [practice code][c12]).

<!-- This is a comment
*   [Lesson 13][l13]: Random numbers ([slides][l13], [`pdf` file][l13-pdf],
    [practice code][c13]).

*   [Lesson 14][l14]: Classes: _user defined classes_ ([slides][l14], [`pdf`
    file][l14-pdf], [practice code][c14]).

*   [Lesson 15][l15]: Classes: _function overloading_, _implicit_ vs _explicit_
    parameters, _member_ vs _non-member_ functions, separate compilation
    ([slides][l15], [`pdf` file][l15-pdf], [practice code][c15]).

    > Also, make sure to checkout this [step by step][steps] implementation of a
    > `RightTriangle` class. It might come in handy when working on your
    > `Rectangle` class. The final version can be found in this [three-file
    > layout][three-file].

*   [Lesson 16][l16]: Vectors ([slides][l16], [`pdf` file][l16-pdf], [practice
    code][c16]).

*   [Lesson 17][l17]: Arrays ([slides][l17], [`pdf` file][l17-pdf], [practice
    code][c17]).

<!-- This is a comment
*   [Lesson 18][l18-pdf]: File streams and `string` streams ([`pdf` long
    version][l18-pdf], [practice code][c18], [`pdf` short version][l18-short]).

    > Feel free to also check out [this old Google slides presentation][l18]
-->

[steps]: code/right_triangle_class/step-by-step
[three-file]: code/right_triangle_class/separate-compilation

[practice]: ../lessons/code
[l1]: lesson01.pdf
[l2]: lesson02.pdf
[l3]: lesson03.pdf

[l4]: https://docs.google.com/presentation/d/e/2PACX-1vSD1F8Rs8al-CVvfZ_WMQnHfjTMrYn7497jjUJsC1A6QRX-tWgpFPCd_N7rwMkkb9L0KjxNyHtiI_jG/pub?start=false&loop=false&delayms=3000
[l5]: https://docs.google.com/presentation/d/e/2PACX-1vQ-hLV4UbW_LN7xle188fp2TMVxhx-teMFpCs-CLMJRX8uW9QODZQyDWjyVEgPsdB17nJgnc4htESIA/pub?start=false&loop=false&delayms=3000
[l6]: https://docs.google.com/presentation/d/e/2PACX-1vR84ZlW3wNV6YMwW1fSDe4Q-XYdBuDSJ7BBjpg6vWlc9tSVhjEpnUTlMXk3bcfr_vaEl6xVdPbwkWD6/pub?start=false&loop=false&delayms=3000
[l7]: https://docs.google.com/presentation/d/e/2PACX-1vQyfqNQdkft0pWiQT_xO2wrwcfifTi9HmaRQVdUBpa8URgVpLhX7DXN3C0FsfqiuFqo8xYF_5DJ8hmw/pub?start=false&loop=false&delayms=3000
[l8]: https://docs.google.com/presentation/d/e/2PACX-1vQDhEs2CKNim0O4WWEnBix6FOxnIn6UKH5CdHeBreToLqNjpFHI0TFe--2gktUALoJueC5Ev_OOKzYE/pub?start=false&loop=false&delayms=3000
[l9]: https://docs.google.com/presentation/d/e/2PACX-1vTWetziVMJy9FrJ29RgkzNEHwYh9TZYZn1bkDBtryUHQ_mOY3idtlgj9SiWxKvIVrcIN88EWMiLgTv6/pub?start=false&loop=false&delayms=3000
[l10]: https://docs.google.com/presentation/d/e/2PACX-1vTfW7tyRXKuT_IelaHuwrN-PrFwZlib-cSR_LZn-xl1XCGaplETvG6AXIDnV1SWC1Q1pXuEmBCdIkYy/pub?start=false&loop=false&delayms=3000
[l11]: https://docs.google.com/presentation/d/e/2PACX-1vRU8BgN2aLCfeX2trVgk_Rjqctj6IY7dQJaCpkpbvq6k6xVAoRfy_gBJaAJ9ciFdfGQphEyQgrqPnxz/pub?start=false&loop=false&delayms=3000
[l12]: https://docs.google.com/presentation/d/e/2PACX-1vT4CQejRb6Ty6aDuwjTsJkK5Jt91JxaA92Ub0XayTgWKY8CyB9UEM-B-xijXxu2trRuKt9CH_hxAeB_/pub?start=false&loop=false&delayms=3000
<!-- This is a comment
[l13]: https://docs.google.com/presentation/d/e/2PACX-1vQS8gtwNahU__x9Jbux8Ql3BpG32CTB-WbxWs4aymaH6x2Le8zfkMm7xL5w12QOalsgokzxaWEDU8bR/pub?start=false&loop=false&delayms=3000
[l14]: https://docs.google.com/presentation/d/e/2PACX-1vRCBbvY88coPNQ0LHuyA-7AQl-ZyMLIHEngar7srd8RPsp6ZlWd9cAmOxdJOUk21BtKw8r9oOyFUmN2/pub?start=false&loop=false&delayms=3000
[l15]: https://docs.google.com/presentation/d/e/2PACX-1vSUOwvLxGNO_2olF4TAvJaG-IbrVZGsRNFX5ApD1xsYDHFmmTNsjLDQwOg6e9SeWGT38eu9JggdKGPR/pub?start=false&loop=false&delayms=3000
[l16]: https://docs.google.com/presentation/d/e/2PACX-1vSUOwvLxGNO_2olF4TAvJaG-IbrVZGsRNFX5ApD1xsYDHFmmTNsjLDQwOg6e9SeWGT38eu9JggdKGPR/pub?start=false&loop=false&delayms=3000
[l17]: https://docs.google.com/presentation/d/e/2PACX-1vSLShBJscGzVU3AWsq7XJ3smnwUSJmPlqr_0EWgHX9kKsrL7FBDPUX6mKc1-64eFGEHeKp1ucCdr6M1/pub?start=false&loop=false&delayms=3000
<!-- This is a comment
[l18]: https://docs.google.com/presentation/d/e/2PACX-1vSTGWhSS0hQN2-cr2bL5nPptzRtFP9e4bV6X1F-WfHWjGLalz0eqxZnYBLpkyBy9TeUk4ODOaVV8ELV/pub?start=false&loop=false&delayms=3000

-->

[c01]: code/01
[c02]: code/02
[c03]: code/03
[c04]: code/04
[c05]: code/05
[c06]: code/06
[c07]: code/07
[c08]: code/08
[c09]: code/09
[c10]: code/10
[c11]: code/11
[c12]: code/12
<!-- This is a comment
[c13]: code/13
[c14]: code/14
[c15]: code/15
[c16]: code/16
[c17]: code/17
<!-- This is a comment
[c18]: code/18
-->
[ccc_time]: code/ccc_files

[l4-pdf]: lesson04.pdf
[l5-tempo-pdf]: lesson05-tempo.pdf
[l6-pdf]: lesson06.pdf
[l7-pdf]: lesson07.pdf
[l8-pdf]: lesson08.pdf
[l9-pdf]: lesson09.pdf
[l10-pdf]: lesson10.pdf
[l11-pdf]: lesson11.pdf
[l12-pdf]: lesson12.pdf
<!-- This is a comment
[l13-pdf]: lesson13.pdf
[l14-pdf]: lesson14.pdf
[l15-pdf]: lesson15.pdf
[l16-pdf]: lesson16.pdf
[l17-pdf]: lesson17.pdf
<!-- This is a comment
[l18-pdf]: lesson18.pdf
[l18-short]: lesson18-short.pdf
-->

[^one]: `ccc` stands for **C**omputing **C**oncepts in **C**++. These classes
  are provided by Horstmann (the author of the textbook), and in particular,
  they are not considered to be _"part of"_ C++.


[l5-tempo]: https://docs.google.com/presentation/d/e/2PACX-1vRqeQCRu7en4lrB4x1VkySjZqkuhwOaDcqMMVrULlQKFQAvKvxsYHgMADbL2yIHDWgkTxBLL3KDKcDN/pub?start=false&loop=false&delayms=30000



---

[Return to main course website](..)
