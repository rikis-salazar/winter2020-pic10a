# Syllabus: Winter 2020 Lecture 4

> If you are reading the online (html) version of this syllabus, [click
> here][pdf] to access a `.pdf` version of this document.[^if-you]

[pdf]: readme.pdf
[^if-you]: The embedded link does not work in the `.pdf` document.

---

## Course information

**Contact:** [`rsalazar@math.ucla.edu`][correo] (write "Pic 10A" in the
subject).

**Time:** 2:00 to 2:50 pm.

**Location:** MS 5200.

**Office hours:** [See CCLE website][CCLE-website].

**Teaching assistant(s):**

|**Section**| **T. A.**     |**Office**| **E-mail**                           |
|:---------:|:--------------|:--------:|:-------------------------------------|
| 4A        | Smith, A. D.  | MS 3905  | [`asmith@math.ucla.edu`][t-a1] |
| 4B, 4C    | Swartworth, W.J. | MS 2350  | [`wswartworth@math.ucla.edu`][t-a2] |

[correo]: mailto:rsalazar@math.ucla.edu
[CCLE-website]: https://ccle.ucla.edu/course/view/19W-COMPTNG10A-4
[t-a1]: mailto:asmith@math.ucla.edu
[t-a2]: mailto:wswartworth@math.ucla.edu


## Course Description

Basic principles of programming, using `C++`; algorithmic, procedural problem
solving; program design and development; basic data types, control structures
and functions; functional arrays and pointers/iterators; introduction to classes
for programmer-defined data types.


## Textbook

You are welcome to use your copy of Horstmann C. & Budd, T. A. Big `C++`, _2nd/3rd
Edition._ John Wiley and Sons, Inc. However, be advised that most of the
material presented during lecture can be found elsewhere. Whenever possible,
lecture notes and/or slides will be prepared, and if applicable, made available
to students.


## Course websites: PIC, CCLE, and MyUCLA

During this quarter I will utilize at least three different websites:

*   [PIC (`www.pic.ucla.edu/~rsalazar/winter2020`)][pic]: This is to be considered
    the _main class website_. Examples, code, slides, handouts, assignment
    descriptions, etc., will be hosted here.
*   [CCLE][ccle]: for class announcements and homework submissions.
*   [MyUcla][myucla]: to report scores/grades.

[pic]: https://www.pic.ucla.edu/~rsalazar/winter2020/
[ccle]: https://ccle.ucla.edu
[myucla]: https://my.ucla.edu

## Reaching me via email

_Please consult CCLE announcements as well as this syllabus before sending me an
email about a policy or procedure as your question(s) might already be answered
here._

If you feel that your question/issue has not been addressed in the documents
listed above, do feel free to send me a message, however keep the following in
mind:

*   I receive a high volume of messages throughout the day; it might be faster
    for you to get the information you seek if you reach out to me in person
    (say during O.H., or right before/after lecture).
*   Messages with special keywords _skip_ my inbox. Use this to your advantage:
    if you add `Pic 10a` to your subject line, your message will find its way
    into a special folder that I check periodically. This should reduce the time
    you have to wait before I reply to it.
*   Messages with special attachments, more specifically text files with
    specific file extensions (_e.g.,_ `.cpp`, `.h`, etc.), as well as messages
    containing some types of `.pdf` attachments, are sent directly to my _trash_
    folder.

    > This inconvenience brought to you by students that are under the
    > impression that deadlines do not apply to them (_e.g.,_ they attempt to
    > _late-submit_ their assignments via email attachments).


## Grading

_Grading method:_ homework assignments (**Hw**), midterm (**Mid**), and final exam
(**Final**).

*   _Homework assignments:_ there will be 6 or 7 homework assignments throughout
    the quarter. **No late homework will be accepted** for any reason, but
    **your lowest homework score will not count** towards the computation of
    your overall score.

*   _Midterm:_ one fifty-minute midterm will be given on **February 3 (Monday
    week 5)** from **2:00 to 2:50 pm** at **TBA**.

*   _Final exam:_ this exam will be given on **Wednesday, March 18** from **11:30
    to 2:30 pm** at **TBA** (click [here][final-tba] for up-to-date information
    about the exam location).  
    **_Failure to take the final exam will result in an automatic F!_**

    The exam will consist of two _mutually exclusive_ sections, and every
    section will be scored separately.

    -   _Section 1_ will be based on the contents discussed during weeks 1
        through 4. **This section is optional**, and can be thought of as a
        midterm do-over.
        
        > Students that either fail to take, or don't do well on the midterm,
        > will have an opportunity to _make up_ for the missed exam, or improve
        > their midterm score. On the other hand, students that do well on the
        > midterm, have the option to skip this section altogether and focus
        > their efforts on the second section of the final.

    -   _Section 2_ will  be based on material discussed after the midterm. In a
        sense, this qualifies as a second midterm, or as a non-cumulative final
        exam. This section **is mandatory** and carries a slightly higher
        weight for purposes of _overall score_ computations.

[final-tba]: https://sa.ucla.edu/ro/public/soc/Results/ClassDetail?term_cd=20W&subj_area_cd=COMPTNG&crs_catlg_no=0010A+++&class_id=157050230&class_no=+004++

Overall scores will be computed using the following formula:

|                                                                        |
|:----------------------------------------------------------------------:|
| 25% **Hw** + 35% _HNS[^one]_ between **Mid** & **Final (Sec 1)** + 40% **Final (Sec 2)** | 

[^one]: HNS = _Highest Normalized Score_. If your scores in the midterm and
  final (section 1) are 45/50 and 50/58, respectively; then your normalized
  scores are 90% and 86.2%. Your _HNS_ is then 90%.

Overall scores determines letter grades according to the table below.

|                       |                      |                    |
|:----------------------|:---------------------|:-------------------|
| A+ (N/A)              | A (93.33% or higher) | A- (90% -- 93.32%) |
| B+ (86.66% -- 89.99%) | B (83.33% -- 86.65%) | B- (80% -- 83.32%) |
| C+ (76.66% -- 79.99%) | C (73.33% -- 76.65%) | C- (70% -- 73.32%) |

The remaining grades will be assigned at my own discretion. Please do not
request exceptions.

> **Please note:** students taking this class on a P/NP basis that attain an
> overall score in the C- range **are not** guaranteed a P letter grade.

**All grades are final when filed by the instructor on the Final Grade Report.**


### Policies and procedures (exams)

Make sure to bring your UCLA ID card to every exam. You will not be allowed to
consult books, notes, the internet, digital media or another student's exam.
Please turn off and put away any electronic devices during the entire duration
of the exam.

**There will be absolutely no makeup midterms under any circumstances.** In
addition this class **does not feature a dual grading schema** for the purposes
of grade computations. However, notice that the final exam design, as well as
the overall score formula make it possible for students obtain a 100% score,
even by missing the midterm.

The midterm exam will be returned to you during discussion section and your TA
will go over the exam at that time. _Any questions regarding how the exam was
graded **must be submitted in writing** with your exam to the TA at the end of
section that day._ No regrade requests will be allowed afterwards regardless of
whether or not you attended section. Please get in touch with me if you
anticipate missing section due to a family emergency or a medical reason.

### Policies and procedures (assignments)

Links to programming assignment descriptions, as well as submission pages, will be
available at the class website once I deem we have covered the material you will
need to successfully complete them. Once a specific assignment is _available to
the class_ you will have **at least one week to complete it**.

Prior to the assignment due dates I usually conduct an informal poll (during
lecture) to determine whether or not an extension might be appropriate. If this
is the case, I will quietly change the CCLE submission settings for the
corresponding assignment. In particular, this means that I will likely not send
an official announcement to the class. You are encouraged to check the CCLE
submission pages on a regular basis to find out about changes (if any) to
assignment due dates.

_In an effort to be fair to all students, messages sent to my email address that
contain either `.cpp`, `.txt`, `.h`, `.hpp`, as well as some `.pdf` attachments
will automatically be deleted from my inbox._

You are responsible for naming your files correctly and you need to make sure
that you submit them through the proper channels. You are free to use your
favorite software but you should make sure you test your projects against one of
the 'official' compilers for the class. These compilers are:

*   The GNU `C++` compiler installed at the laguna web server[^five], with
    compilation flags `-Wall`, and `-std=c++11`.[^six]
*   Microsoft Visual Studio 2017 as available in the PIC lab (MS 2000).

[^five]: At the time of this writing this compiler is
  `g++ (SUSE Linux) 4.8.3 20140627`.
[^six]: Special software is needed to access this compiler. A handout with
  instructions on how to test your projects using this compiler will be provided
  to you at a later time.

Failure to test your code against at least one the compilers listed above might
result in a 0/20 score, as well as _"Failed compilation report"_ feedback
message on your MyUcla records. If you find yourself in this scenario, you can
either:

a.  contact the instructor in person to have your project fixed and recompiled,
    or
b.  submit a regrade request to [`pic10a.grader@gmail.com`][grader].

[grader]: mailto:pic10a.grader@gmail.com

In either case, the contact should be made within 7 days of the date the class
is notified that the corresponding assignment has been graded. More details
about regrades will be found in the handouts section of the course website.


## About coding integrity

You are encouraged to discuss aspects of the course with other students as well
as homework assignments with others in general terms (_i.e.,_ general ideas and
words are OK but code is not OK). If you need specific help with your programs
you may consult any of the course TAs, or the course instructor. Copying major
parts of a program or document written by someone else (_e.g.,_ code found
online) should be avoided in general. Under certain circumstances --for example,
when starting a project from a template, or when trying to implement an
idea/algorithm presented to you during lecture/DS, and/or found online--,
copying is not _frowned upon_, but actually encouraged. If you believe you this
scenario applies to you, you should disclose the source/author of the code you
are including in your project. For example, you can write a comment along the
lines of

~~~~~ {.cpp}
// The following code is taken from www.some.web-site.edu/found-it/online
// where "Elmer H. Omero" uses it to solve the P = NP problem.
~~~~~

## Accommodations

If you need any accommodations for a disability, please contact the UCLA
CAE[^two] [(`www.cae.ucla.edu`)][CAE]. Make sure to let me know as soon as
possible about necessary arrangements that need to be made.

[CAE]: https://www.cae.ucla.edu
[^two]: Center for Accessible Education

|                                                      |
|:----------------------------------------------------:|
| Course Syllabus Subject to Update by the Instructor. |

