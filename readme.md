# PIC 10A: Introduction to programming (spring 2020)

Welcome to the class website. Here you will find handouts, slides, and code
related to the concepts we discuss during lecture.

## Sections

*   [The class syllabus][syllabus] ([`.pdf` version][syllabus-pdf])
*   [Lessons: pdf files & Google slide presentations][lessons]
*   [Class assignments][hw]
*   [Practice code (by lesson number)][code]
*   [Miscellaneous info: Horstmann graphics & regrades][hand2]
*   [Other: Walk troughs, links & handouts][handouts]: includes practice
    material for midterms.

> At some point during this quarter, the last section above might no longer be
> hosted at CCLE. If things go as planned, the transition should be smooth and
> you might not even notice the changes.

[syllabus]: syllabus/
[syllabus-pdf]: syllabus/readme.pdf
[handouts]: https://ccle.ucla.edu/course/view/20W-COMPTNG10A-4?section=2
[lessons]: lessons/
[code]: lessons/code
[hw]: assignments/
[hand2]: handouts/
[not-found]: https://www.kualo.co.uk/404


## Additional resources

Here are other sites associated to our course:

*   [The CCLE class website][CCLE]: this will be used only for class
    announcements, and homework submissions.
*   [Discussion section material (examples, code, etc.)][ds]: Your TA might
    decide to post discussion material for the beneffit of the class as a whole.

[CCLE]: https://ccle.ucla.edu/course/view/20W-COMPTNG10A-4
[ds]: https://ccle.ucla.edu/course/view/20W-COMPTNG10A-4?section=6
