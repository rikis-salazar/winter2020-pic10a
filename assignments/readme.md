# Homework assignments

This is the place to find homework assignment descriptions. Please note that you
will not be able to submit your code via this setup. Instead every assignment
will contain a link to its corresponding CCLE assignment submission entry.

*   [Assignment 0: _hello favorite world_][HW0].
*   [Assignment 1: _the journey to Acapulco_][HW1].
*   [Assignment 2: _some conversion issues_][HW2].
*   [Assignment 3: _a 'gato' game_][HW3].
*   [Assignment 4: _the trip back to Ciudad de México_][HW4].
*   [Assignment 5: _a Monte Carlo style roulette_][HW5].
*   [Assignment 6: _a `Rectangle` class_][HW6].


[HW0]: https://ccle.ucla.edu/mod/assign/view.php?id=2725154
[HW1]: hw1/
[HW2]: hw2/
[HW3]: hw3/
[HW4]: hw4/
[HW5]: hw5/
[HW6]: hw6/

---

## Compilation results

As demonstrated during lecture, the compilation of all assignments is taken care
of via a set of _python_ utilities that invoke the command `cl` (the compiler
used by Visual Studio). After the due date of every assignment, the compilation
results will be saved in a text file and will be posted here. If your submission
did not compile, check your _My.ucla_ account; the corresponding assignment
submission category will contain an URL address with information about the
reason your project failed to compile. See also the [class syllabus][syll]
and/or [this handout][handout] for information about regrade requests.

*   [Assignment 1 compilation results][log1]: the deadline to request a regrade
    is February 12.  
*   [Assignment 2 compilation results][log2]: the deadline to request a regrade
    is March 2.  
*   [Assignment 3 compilation results][log3]: the deadline to request a regrade
    is March 2.  
*   [Assignment 4 compilation results][log4]: the deadline to request a regrade
    is March 8.  

[log4]: hw4-compilation-log.txt
[log3]: hw3-compilation-log.txt
[log2]: hw2-compilation-log.txt
[log1]: hw1-compilation-log.txt
[handout]: ../handouts/regrades
[syll]: ../syllabus

---

[Return to main course website][PIC]

[PIC]: ..

