# Coding assignment: a _Monte Carlo_ style roulette.

| [ ![pensando.png](pensando.png) ][pensando] |  
|:-----:|  
| Figure 1: _Don Ramón_ and _El Chavo_ thinking about a plan to get money for _Valentine's day_. |

[pensando]: https://www.youtube.com/watch?v=seEN_WD-pdA


## The backstory

Finally, _Don Ramón_ and _El Chavo_ are back home. They made it just in time for
[_El día del amor y la amistad_][san-valentin] (which translates literally to
_The day of Love and Friendship_, but it is just another way to refer to
_Valentine's day_). _El Chavo_ came back with no money at all, whereas _Don
Ramón_ managed to come back with 19.28 pesos (1 USD). _Don Ramón_ needs money,
and he needs it quick if he hopes to buy a nice present for _Gloria_, her
neighbor. While thinking about a plan (see picture above), _Don Ramón_ recalls
_el chofer de paquetería_ telling them about the time he went to
_[Europa][europa], [Michigan][michigan]_. (Wait!  Whaaat?)

> Note: It turns out that people in _México_ like to replace the name of a place
> with another one that sounds more or less the same: he was referring to
> _[Uruapan][uruapan], [Michoacán][michoacan]_.

Anyway, back into the story...

The delivery truck driver told them there was a casino in that city. Moreover, a
casino _[croupier][croupier]_ confided him once that the [American style
roulette][american] was the best game for a casino owner: that is, in the long
run a player loses the most compared to any other casino game.

Based on this conversation, _Don Ramón_ figured that he could start his own
roulette table. However, since time is not on his side he needs to have as many
players as possible bet on his improvised casino. After a bit of research, he
decides to try a modified [_Monte Carlo_ style roulette][monte-carlo]. According
to his thinking, this modified version will be a lot more popular, and hence, he
will make more money.

Unfortunately, as you know very well by now, _'monchito'_ (_Don Ramón's_
nickname) is not very good with math. He might have chosen a set of rules that
will make him lose money instead!!! If only there was a device that could show
him the future and tell him whether he will make or lose money.

> _Y ahora, ¿quién podrá ayudarlo?_

[san-valentin]: https://en.wikipedia.org/wiki/Valentine%27s_Day
[europa]: https://goo.gl/maps/w7Nnq
[michigan]: https://goo.gl/maps/js5HJ
[uruapan]: https://goo.gl/maps/VrG6x
[michoacan]: https://goo.gl/maps/VrG6x
[american]: https://www.roulettesites.org/games/american/
[monte-carlo]: https://www.roulettesites.org/games/european/
[croupier]: https://en.wikipedia.org/wiki/Croupier


## The assignment

You are to run a simulation of a casino game; more specifically a _modified_
Monte Carlo roulette. According to Wikipedia:

> In _Monte Carlo_, a roulette wheel has 37 slots numbered 0, 1, 2, ... , 36.
> The 0 slot is green, and half of the remaining 36 slots are red and half are
> black. A croupier (dealer) spins the wheel and throws an ivory ball. **If you
> bet 1 peso on red and a 0 turns up, then, depending upon the casino**
> (emphasis mine), one [or more] options may be offered...

For example, the following describes a typical set of rules:

_If you bet 1 peso on red and a 0 turns up your bet is put_ "in prison"_, which
we will denote by_ P1.

*   _If red comes up on the next turn, you get your bet back (but you don't win
    any money)._
*   _If black or 0 comes up, you lose your bet._

On a similar note, the following describes another typical, but not so simple
set of rules:

_If you bet 1 peso on red and a 0 turns up your bet is put_ "in prison"_,
denoted by_ P1 _as before._

*   _If red comes up on the next turn you get your bet back (net gain of 0
    pesos)._
*   _If black comes up on the next turn then you lose your bet (net loss of 1
    peso)._
*   _If a 0 comes up on the next turn, then your bet is put into_ "double
    prison"_, which we will denote by_ P2.
*   _If your bet is in double prison, and..._

    -   _...if red comes up on the next turn, then your bet is moved back to
        prison_ P1 _and the game proceeds as before._
    -   _...if black or 0 come up on the next turn, then you lose your bet (net
        loss of 1 peso)._

> **Fun fact**
>
> It can be mathematically shown that on average:
>
> *   with the first set of rules, a player loses per game 0.0139 pesos [1.39
>     cents], and
> *   with the second set of rules, a player loses 0.0137 pesos [1.37 cents] per
>     game.
>
> For comparison purposes:
>
> *   for a double zero American style roulette, the average loss is 0.0526
>     pesos [5.26 cents], and
> *   for a triple zero roulette (_c.f., The Venetian_ in _Las Vegas_ since the
>     year 2016) the average loss is 0.0769 pesos [7.69 cents].

_Don Ramón_, thinking that a landing on green while in _double prison_ is such a
highly unlikely event, decides to offer a big payout for a player in this rare
scenario; he pulls the number 684 out of nowhere and settles on the following
modified set of rules:

*   _If 1 peso is bet on red and a red turns up: the player gets the bet back
    plus 1 additional peso, resulting in a_ net gain of 1 peso.
*   _If 1 peso is bet on red and a black turns up: the player loses the bet,
    resulting in a_ net loss of 1 peso.
*   _Lastly, if 1 peso is bet on red and a 0 (green) turns up, the bet is put in
    prison_ P1.
    +   _If while in_ P1 _a red comes up: the player gets the bet back (net gain
        of 0 pesos)._
    +   _If while in_ P1 _a black comes up: the player loses the bet (net loss
        of 1 peso)._
    +   _If while in_ P1 _a 0 comes up: the bet is put in double prison_ P2.
        *   _If while in_ P2 _a red comes up: the bet is moved back to prison_
            P1 _and the game proceeds as before._
        *   _If while in_ P2 _a black comes up: the player loses the bet (net
            loss of 1 peso)._
        *   _If while in_ P2 _a 0 comes up:_ **the player wins big time!!!**
            _(net gain of 684 pesos)._


### Where to start & what to do

This is a rather complex assignment, however you do not need to start from
scratch. [Download this file (`hw5.cpp`)][template] and simply add the needed
code so that the resulting program is an accurate representation of _Don
Ramon's_ version of a _Monte Carlo_ style roulette. After you are done adding
the code, _run a bunch of experiments_ to determine how much money on average a
player wins/loses under Don Ramon's rules.

The estimated value, as well as your name and a brief description of what your
program does should to be present at the top of your homework file. For example,
you can write:

~~~~~ {.cpp}
// Ricardo Salazar, February 14, 2015.
// This program simulates a Monte Carlo roulette with special rules.
//
// On average, every game a player wins 0.005 pesos ( 0.5 cents ).
~~~~~

[template]: hw5.cpp


## What is this assignment about?

As you can see in the `hw5.cpp` template, the structure has already been laid
out for you. This means that you do not have to create new functions nor
procedures. You just need to fill in the blanks with code that produces the
expected results. This is essential preparation for implementation of classes,
where typically one has a good idea of what the constructors and member
functions are; with this information, the interface of the class is then not
very hard to write. After the interface is put in place, the programmer then
needs to fill in the blanks and write code that produces the desired results.

This assignment also provides examples of how enumerated types as well as random
numbers can become powerful tools to analyze real world problems.


## What is your program expected to do?

Your program is expected to help you come up with the correct answer to the
question of _how much money on average a player will win or lose?_ When
simulating the game of roulette you are free to choose how many times you want
to repeat a given experiment. However, once you are satisfied with your program
and have settled for an answer to the posed question, you need to:

*   either comment, or remove the line that seeds the random number generator;
    and
*   adjust your program so that exactly 15000 rounds (from bet, to money update)
    are played, and useful information is displayed to the console.

Below you will find some screenshots corresponding to a program I wrote that
simulates a Monte Carlo roulette with the second set of rules described above.

| ![monte-carlo.png](monte-carlo.png) |  
|:-----:|  
| Figure 2: Three runs of a _Monte Carlo_ roulette simulation. |  


Notice that the third time I ran the program, the simulation was not in line
with the theoretical result (average loss of 0.0137, see fun fact above).
This means that you might need to run your simulations more than 15000 times
several times before you decide to settle on an answer.


## Submission

Place your code in a source file labeled `hw5.cpp` (all lowercase). If your file
is not named exactly like this, your homework might not be graded. Your code
should **contain useful comments** as well as **your name**, **the date**, and a
**brief description of what the program does**. Upload your file to the
[assignments section of the CCLE website][ccle-hw]. The file will be
automatically collected at the date and time listed in _"submission status"_
table at the bottom of the CCLE assignment description corresponding to this
project.

[ccle-hw]: https://ccle.ucla.edu/course/view/20W-COMPTNG10A-4?section=4

## Grading rubric

| **Category** | **Description** | **Points** |  
|:---------------|:------------------------------|:----------:|  
| **Correctness** | The programs compiles (i.e., no errors were introduced). 15000 games are simulated and the money left is displayed. | 4 |  
| **Simulation** | Your program correctly simulates a game of Monte Carlo roulette with _Don Ramon’s_ set of rules described above. It is important that you **do not seed** the random number generator! This is so that all programs that we test produce the same output (assuming they are implemented correctly). | 4 |  
| **Average loss/win** | The average loss/win is computed accurately. Although it is very hard to obtain the actual answer with a simulation, by performing many experiments you should be able to get a fairly good idea of what the correct answer is. | 4 |  
| **Solution** | The code is efficient but easy to follow. No additional functions are created and the ones already there are used appropriately. | 4 |  
| **Style** | Variable names, comments, indentation, etc. | 4 |  
| **Total** | | 20 |  


---

[Return to main course website][PIC]

[PIC]: ../..
