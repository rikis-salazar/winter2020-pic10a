#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

enum color_t { RED, BLACK, GREEN };
enum status_t { NORMAL, PRISON1, PRISON2 };

// Change the value to fit your testing purposes
const int RUNS = 10;


// stub: returns RED
color_t getColor(int r){
    // --STATEMENTS--

    // change when function is ready
    return RED;
}


// stub: returns 1
int spinRoulette(status_t s){
    // Generate random number representing a possible slot in a Monte Carlo
    // roulette and obtain the color of the corresponding slot.
    // --STATEMENTS--


    // Implement rules of the game when 'bet is free'. The next stage is decided
    // based on the color of the slot where the ball lands:
    // -   if it lands on GREEN bet goes to prison1. Display the message: "0
    //     Green! Prison."
    // -   if it lands on BLACK, the bet is lost. Display the random number
    //     generated, followed by the message: " Black! Lose."
    // -   if it lands on RED, that's a win. Display the random number
    //     generated, followed by the message: "Red! Win."
    //
    // Notice that for simulations purposes (once you are certain your program
    // works) you can skip the displaying of messages and focus on the money
    // won/lost.  You can then restore the messages before you upload your
    // program to CCLE.
    if ( s == NORMAL ){
        // --STATEMENTS--
        cout << " Red. Win!" << endl;
        // --STATEMENTS--
    }

    // Implement the rules of the game when 'bet is in prison 1' see the
    // assignment description for the actual rules I want you to simulate.
    // -   if it lands on GREEN ...  Display "0 Green ..."
    // -   if it lands on BLACK ...  Display the random number generated,
    //     followed by ...
    // -   if it lands on RED ...  Display the random number generated, followed
    //     by ...
    if ( s == PRISON1 ){
        // --STATEMENTS--
    }

    // Implement the rules of the game when 'bet is in prison 2' see the
    // assignment description for the actual rules I want you to simulate.
    // -   if it lands on GREEN ...  Display "0 Green ..."
    // -   if it lands on BLACK ...  Display the random number generated,
    //     followed by ...
    // -   if it lands on RED ...  Display the random number generated, followed
    //     by ...
    if ( s == PRISON2 ){
        // --STATEMENTS--
    }

    // Change the return value when function is ready.
    return 1;
}


// stub: displays "Bye!"
int main(){
    // Line that seeds the random number generator. Uncomment for testing
    // purposes. Comment again or delete when you are ready to submit your
    // program.
    //
    // srand( static_cast<int>( time(0) ) );
    //
    // IMPORTANT: 
    //     Before your turn in your program you need to remove the line above.
    //     This is so that we can determine whether or not your program
    //     correctly simulates a Monte Carlo roulette.


    // Initializations
    // --STATEMENTS--
    for ( int n=1; n <= RUNS ; n++ ) ; // <-- remove the ; when ready 
        // --STATEMENTS--
   
    // Display statistics:
    // --STATEMENTS--
    cout << "Bye!" << endl; // <-- remove [or not] when ready

    return 0;
}
