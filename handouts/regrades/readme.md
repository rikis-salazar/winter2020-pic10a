# Handout: requesting a regrade

In order to request a regrade you need to know

1.  what is wrong with your submission, and
1.  how to fix it.

If your project fails compilation, the score reported on your [_My.ucla_][my]
account will be 0. In addition, an URL address will point you to the issue(s).
For example, the URL address [`http://ix.io/1xPw`](http://ix.io/1xPW) contains
the following error messages:

~~~~~  {.gcc}
main.cpp: In function ‘int main()’:
main.cpp:6:5: error: ‘cout’ was not declared in this scope
     cout << "Hola, mundo.\n";
     ^
main.cpp:6:5: note: suggested alternative:
In file included from main.cpp:1:0:
/usr/include/c++/5/iostream:61:18: note:   ‘std::cout’
   extern ostream cout;  /// Linked to standard output
                  ^
~~~~~  

which indicates that the compiler does not recognize `cout` as either a
statement, or an object.

> Note: A possible way to fix this is by adding `using namespace std;` somewhere
> before line 6 in `main.cpp`.

Once you have spotted the likely source of the error you should then **test your
solution against an official course compiler[^one]**. Remember that even if a
project works in your device, it might still fail to work on other devices.

Assuming you have fixed the error you should then **request the regrade in
person**: simply talk to me (the instructor) right before/after lecture, or
during office hours (check CCLE for location and times). I usually carry around
all project submissions with me. If the fix is simple I can edit your files on
the spot; if more work is needed, I can provide directions on how to make the
files available to me and/or the reader for this course.


[^one]: Check the [course syllabus][syll] for information about official
  compilers for this course.

---

[Return to main course website][PIC]

[my]: https://my.ucla.edu/
[syll]: ../../syllabus
[PIC]: ../..
