# Horstmann Graphics Project Setup

## One copy to rule them all

This setup requieres one extra step (in addition to the ones listed at [the PIC
website][PIC-GEN]).

In return, you no longer have to make a copy of the five `ccc` files required
for graphics projects. Instead you can have **only one** copy stored in a
convenient location (say a folder named `ccc_graphics` in your Dektop), and use
it for **all of your graphics projects**.

### The setup

Go through all of the steps listed at [the PIC website][PIC-GEN] but skip step
6. For your convenience here are those steps:

1.  Download the graphics libraries.
1.  Unzip `ccc_graphics.zip`.
1.  Create a new windows project.
1.  Set the character code.
1.  Create a new source file.
    -   While you are at it, write some test code. Feel free to download any
        example from the [course PIC website][PIC-10a].
1.  ~~Move the ccc files to the same directory as your source file.~~ (Skip this
    step!!!)
1.  Add the `ccc` files to your Header Files.
1.  Add the unzipped `ccc_graphics` folder (in your Desktop) to the Visual
    Studio list of Include directories.

To walk you through the process [watch the companion video][video] **after
reading the notes below**.

> **Notes:**
>
> 1.  The video starts after step 5 above has been completed.
> 1.  In step 7, the menu that appears when the project folder is
>     _right-clicked_ was not correctly captured by my screencast software. The
>     menu entry that you need is _Add_ &rarr; _Add existing item_.
> 1.  In step 8 the menu that appears when the project folder is _right-clicked_
>     was not correctly captured by my screencast software. The menu entry that
>     you need is _Properties_ (which is the same one used in step 4).
> 1.  In step 9 (described in the video) blah, blah, blah... _Debug_ &rarr;
>     _Start without debugging_.

[PIC-GEN]: https://www.pic.ucla.edu/how-to-create-a-c-graphics-application/
[PIC-10a]: https://www.pic.ucla.edu/~rsalazar/pic10a
[video]: https://www.youtube.com/watch?v=mYTqTv9VBO8


---

[Return to main course website][PIC]

[PIC]: ../..
