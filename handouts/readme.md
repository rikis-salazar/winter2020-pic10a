# Miscellaneous info: Horstmann graphics, regrades & how-to guides/demos 

This section is for miscellaneous material that does not quite fit in other
sections.


## Regrades

*   [Requesting a regrade][link1].


## Horstmann graphics

**Microsoft Windows**

*   [Visual Studio 2017 (written instructions)][link2]

    > **Important:**
    >
    > Visual Studio 2017 is has been replaced by _Visual Studio 2019_ at the PIC
    > lab and as a result the instructions above need updating:
    >
    > *   In _"Step 1 Download the graphics libraries"_, **do not** click on the
    >     link therein: 2 of those files need a minor modification (see
    >     _things-to-double-check_ below); instead use [the set of files I keep
    >     at CCLE][files], they should work without any additional changes.
    > *   In _"Step 4 Set the character code"_, the property _"Character set"_
    >     **can no longer be found under** _"Configuration Properties =>
    >     General"_; **instead look under** "Configuration Properties =>
    >     Advanced".
    >

    If after following the instructions your project still does not compile,
    here are some things for you to consider:

    *   The `.zip` file provided by the PIC website contains two `.cpp` files
        that use the _deprecated_ instruction `sprintf`; this instruction needs
        to be replaced with the newer instruction `sprintf_s`.
    *   Make sure the character set configuration property is _set_ to **Not
        set**.
    *   Make sure **all of the 6 files** that are needed for your project
        (`ccc_win.h`, `ccc_shap.cpp`, `ccc_shap.h`, `ccc_msw,h`, `ccc_msw.cpp`,
        as well as the one that contains your `ccc_win_main()` function), are
        located in the folder that visual studio created for your project.


*   [Visual Studio 2017 (video)][link3]: based on the written instructions in
    the link above.

    > **Important:**
    >
    > Please read the notes in the previous _bullet point_.

    > Note:
    >
    > The setup described in the previous two links requiere you to _move_ a set
    > of downloaded files into a special folder created by Visual Studio. This
    > means that every time you create a new project, [a copy of] those 5 files
    > will have to be redownloaded and _moved_ (or _copied_) over to a different
    > location.

*   [Visual Studio 2017 (handout + short video)][link4]: a slightly more
    difficult setup that does not requiere extra copies of the `ccc` files.

    > **Important:**
    >
    > Please read the notes above regarding Visual Studio 2019.

**Mac OS X**

*   [Visual Studio Code (written instructions)][link5]
*   [Visual Studio Code (video)][link6]: based on the written instructions in
    the link above.

    > **Note:**
    >
    > The links above refer to Visual Studio Code, which is not the same as
    > Visual Studio for Mac.

*   [Xcode (video)][link7]: based on instructions sent to some of ~~you~~ my
    Winter 2019 students by _Josh Keneda_.

    > **Note:**
    >
    > The video turned out to be a bit low resolution. The info listed here
    > corresponds to the _strings_ you need to enter when configuring your
    > project:
    >
    > -   In _"Header Search Paths"_, type `/opt/X11/include`.  
    > -   In _"Library Search Paths"_, type `/opt/X11/lib`.  
    > -   In _"Other Linker Flags"_, type `-lX11`. That is _dash_, _lower-case
    >     ell_, followed by _capital ex_, followed by _eleven_.

[link1]: regrades/
[link2]: https://www.pic.ucla.edu/how-to-create-a-c-graphics-application/
[link3]: https://www.youtube.com/watch?v=e9HA1uiRCeA
[link4]: vs2017-no-copies/
[link5]: vs-code-for-mac/
[link6]: https://youtu.be/NnxFKHezwjw
[link7]: https://www.youtube.com/watch?v=JWupRku64QQ
[files]: https://ccle.ucla.edu/mod/resource/view.php?id=2772493

---

[Return to main course website][PIC]

[PIC]: ..

